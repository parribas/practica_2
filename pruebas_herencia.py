import unittest
from Ejercicio_herencia import Persona, Paciente, Médico

class Test_Persona(unittest.TestCase):
#Creamos el test para la clase Persona, y comprobamos que nos devuelva la informacion correcta

    def test_str(self):
        persona1 = Persona("Berta", "Babiano", "4/may/2004", "55678320J")
        self.assertEqual(str(persona1), "Nombre y apellidos: Berta Babiano, Fecha de nacimiento: 4/may/2004, DNI:55678320J")

class Test_Paciente(unittest.TestCase):
#Creamos el test del Paciente y comprobamos que nos devuelve bien el historial

    def test_ver_historial_clinico(self):
        paciente1 = Paciente("Cáncer", "Adrián", "Prieto", "27/nov/2005", "5372002J")
        self.assertEqual(str(paciente1.ver_historial_clinico()), "El historial clínico de Adrián es: Cáncer")

class Test_Médico(unittest.TestCase):
    #Y creamos el test para la clase Medico y vemos que nos devuleve la agenda asignada
    def test_consultar_agenda(self):
        médico1 = Médico("neurocirujano", "2, cirujía y consulta", "Adriano Pablo", "González", "23/nov/2005", "345678945L")
        self.assertEqual(str(médico1.consultar_agenda()), "Las citas de Adriano Pablo son: 2, cirujía y consulta")

if __name__ == '__main__':
    unittest.main()
