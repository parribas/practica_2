class Persona:
    def __init__(self, n, a, f, d):
        self._nombre = n
        self._apellidos = a
        self._fecha = f
        self._DNI = d
    #creamos la clase Persona con los atributos que nos piden

    def __str__(self):
        return f'Nombre y apellidos: {self._nombre} {self._apellidos}, Fecha de nacimiento: {self._fecha}, DNI:{self._DNI}'
    
    def setNombre(self):
        self._nombre = n
    
    def getNombre(self):
        return self._nombre

    def setApellidos(self):
        self._apellidos = a

    def getApellidos(self):
        return self._apellidos
    
    def setFecha(self):
        self._fecha = f
    
    def getFecha(self):
        return self._fecha
    
    def setDNI(self):
        self._DNI = d
    
    def getDNI(self):
        return self._DNI
    


class Paciente(Persona):
    def __init__(self, h, n, a, f, d):
        super().__init__(n, a, f, d)
        self.historial_clínico = h

    def ver_historial_clinico (self):
        return f'El historial clínico de {super().getNombre()} es: {self.historial_clínico}'
#creamos la clase Paciente, incluyendo el historial clinico

class Médico (Persona):
    def __init__ (self, e, c, n, a, f, d):
        super().__init__(n, a, f, d)
        self.especialidad = e
        self.citas = c
    
    def consultar_agenda (self):
        return f'Las citas de {super().getNombre()} son: {self.citas}'
#creamos la clase Medico, con consultar agenda para las citas

paciente1 = Paciente("Cáncer", "Adrián", "Prieto", "27/nov/2005", "5372002J")
print(paciente1)
paciente2 = Paciente("Alergias estacionales graves", "Berta", "Babiano", "4/may/2004", "5647890N")
print(paciente2)
medico1 = Médico("neurocirujano", "2, cirujía y consulta", "Adriano Pablo", "González", "23/nov/2005", "345678945L")
print(medico1)

print(paciente1.ver_historial_clinico())
print(medico1.consultar_agenda())






